import React, { SelectHTMLAttributes } from 'react';
import './styles.css';

interface IProps extends SelectHTMLAttributes<HTMLSelectElement> {
    label?: string
}

const Select: React.FC<IProps> = ({ label, children, ...attrs }) => (
    <div className="form-div">
        {label && <label className="form-label">{label}</label>}
        <select className="form-item form-select" {...attrs}>
            {children}
        </select>
    </div>
);

export default Select;