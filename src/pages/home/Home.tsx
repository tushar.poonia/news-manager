import { useEffect, useState } from 'react';
import db from "../../db.json";
import BoardContext from '../../utils/BoardContext';
import api from '../../utils/api';
import { API_BASE_URL } from '../../constants';
import {
    IBoard,
    IBoardContent
} from '../../interfaces';
import {
    Loader,
    NewsBoard,
    NewsBoardSelector,
    NewsCreator
} from '../../components';
import './styles.css';

const Home = () => {
    const [boards, setBoards] = useState<IBoard[]>([]);
    const [boardId, setBoardId] = useState("");
    const [boardContent, setBoardContent] = useState<IBoardContent>({});
    const [fetchingBoards, setFetchingBoards] = useState(false);
    const [fetchingBoardContent, setFetchingBoardContent] = useState(false);
    const [isError, setIsError] = useState('');

    useEffect(() => {
        fetchBoards();
    }, []);

    useEffect(() => {
        fetchBoardContent(boardId);
    }, [boardId]);

    const onSelectBoard = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setBoardId(e.target.value);
    }

    const fetchBoards = async () => {
        setFetchingBoards(true);
        const url = `${API_BASE_URL}board`;
        const { error, data, ok, statusText } = await api.get(url);
        if (error || !ok) {
            setIsError(statusText);
            setFetchingBoards(false);
            return;
        }
        setBoards(data);
        if (data?.[0]?.id) {//select first board's id
            setBoardId(data[0].id);
        }
        setFetchingBoards(false);

        if (false) {
            setTimeout(() => {
                setBoards(db.boards);
                setFetchingBoards(false);
                //select first id
                setBoardId(db.boards[0]["id"]);
            }, 1000);
        }

    }

    const fetchBoardContent = async (boardId: string) => {
        if (isError) {
            setIsError('');
        }
        if (!boardId) {
            return;
        }
        setFetchingBoardContent(true);
        const url = `${API_BASE_URL}board/${boardId}/news`;
        const { error, data, ok, statusText } = await api.get(url);
        if (error || !ok) {
            setIsError(statusText);
            setFetchingBoardContent(false);
            return;
        }
        setBoardContent(data);
        setFetchingBoardContent(false);


        if (false) {
            setTimeout(() => {
                setBoardContent(db.boardContent);
                setFetchingBoardContent(false);
            }, 1000);
        }
    }

    return (
        <div className="home" data-testid="home">
            {fetchingBoards ? <Loader /> :
                <NewsBoardSelector
                    boards={boards}
                    onSelectBoard={onSelectBoard}
                />}
            <BoardContext.Provider value={{ boardId, fetchBoardContent }}>
                {fetchingBoardContent ? <Loader /> :
                    isError ?
                        <div className="error-ctr">
                            <h2>{isError}</h2>
                            <img src="https://c.tenor.com/76XxFDBUu48AAAAC/frustrated-mad.gif" />
                        </div> :
                        <>
                            <NewsCreator boards={boards} boardId={boardId} />
                            <NewsBoard boardContent={boardContent} />
                        </>
                }
            </BoardContext.Provider>
        </div>
    );
}

export default Home;