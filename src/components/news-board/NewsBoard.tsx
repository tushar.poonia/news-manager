import { IBoardContent } from '../../interfaces';
import { NewsRow } from '..';
import './styles.css';

interface IProps {
    boardContent: IBoardContent
}

const NewsBoard: React.FC<IProps> = ({ boardContent }) => (
    <div className="news-content">
        {
            Object.entries(boardContent).map(([heading, newsArr], key) => (
                <NewsRow
                    key={key}
                    heading={heading}
                    newsArr={newsArr}
                />
            ))
        }
    </div>
);

export default NewsBoard;