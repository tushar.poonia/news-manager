import { useState } from 'react';
import './App.css';
import { Login, Home } from './pages';
import { Header } from './components';
import { logoutLocal, loginLocal, getUserEmail } from './utils/commonFunctions';

const App = () => {
  const [currUser, setCurrUser] = useState(getUserEmail);

  const login = (email: string) => {
    setCurrUser(email);
    loginLocal(email);
  }

  const logout = () => {
    setCurrUser("");
    logoutLocal();
  }

  return (
    <div className="App">
      <Header currUser={currUser} logout={logout} />
      {currUser ? <Home /> : <Login login={login} />}
    </div>
  );
}

export default App;