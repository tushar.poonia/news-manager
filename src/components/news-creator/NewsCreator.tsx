import { ChangeEvent, FormEvent, useState, useContext, useEffect } from 'react';
import { IBoard, INews } from '../../interfaces';
import { getUserEmail, isValidEmail, createUuid } from '../../utils/commonFunctions';
import useForm from '../../utils/hooks/useForm';
import { API_BASE_URL } from '../../constants';
import BoardContext from '../../utils/BoardContext';
import api from '../../utils/api';
import {
    TextInput,
    TextArea,
    Select,
    Button,
} from '../atomic';
import './styles.css';

interface IProps {
    boards: IBoard[],
    boardId: string
}

const NewsCreator: React.FC<IProps> = ({ boards, boardId }) => {
    const { form, onChangeForm, resetForm, reInitForm } = useForm({
        boardId,
        "title": "",
        "imageURL": "",
        "description": "",
        "status": "draft",
        "author": getUserEmail(),
    });
    const [open, setOpen] = useState(false);

    useEffect(() => {
        reInitForm();
    }, [boardId]);

    const [emailError, setEmailError] = useState("");
    const { boardId: boardIdContext, fetchBoardContent } = useContext(BoardContext);

    const onChange = (e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLSelectElement> | ChangeEvent<HTMLTextAreaElement>) => {
        onChangeForm(e);
        setEmailError("");
    };

    const onSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (!isValidEmail(form.author)) {
            setEmailError("Invalid Email");
            return;
        }
        const body = {
            ...form,
            id: createUuid(),
            createdAt: new Date().toISOString()
        }
        createNews(body);
        resetForm();
    }

    const createNews = async (news: INews) => {
        const { error, ok, statusText, data } = await api.post(`${API_BASE_URL}news`, news);
        if (error || !ok) {
            alert(statusText);
            return;
        }
        if (boardIdContext === data.boardId) {//refetch data if news is created in the same news board
            refetchData();
        } else {
            setOpen(false);
        }
    }

    const refetchData = () => {
        if (boardIdContext && fetchBoardContent) {
            fetchBoardContent(boardIdContext);
        }
    }

    return (
        <div className="creator">
            <div onClick={() => setOpen(o => !o)} className={`creator-head`}>
                <p>Create News</p>
                <span className={`arrow${open ? ' open' : ''}`}>^</span>
            </div>
            {
                open &&
                <div className="creator-form shadow">
                    <form onSubmit={onSubmit}>
                        <Select
                            label="Board"
                            name="boardId"
                            onChange={onChange}
                            value={form.boardId}
                        >
                            {boards.map((board, key) => (
                                <option
                                    key={key}
                                    value={board.id}
                                >{board.name}</option>
                            ))}
                        </Select>
                        <TextInput
                            label="Author"
                            name="author"
                            onChange={onChange}
                            value={form.author}
                            error={emailError}
                        />
                        <TextInput
                            label="Title"
                            name="title"
                            onChange={onChange}
                            value={form.title}
                        />
                        <TextArea
                            label="Description"
                            name="description"
                            onChange={onChange}
                            value={form.description}
                        />
                        <TextInput
                            label="Image URL"
                            name="imageURL"
                            onChange={onChange}
                            value={form.imageURL}
                        />
                        <Button addClass="form-btn" text="Create" />
                    </form>
                </div>
            }
        </div>
    );
}

export default NewsCreator;