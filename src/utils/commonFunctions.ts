import { EID_KEY } from "../constants";

export const isValidEmail = (email: string) => {
    if(!email)
        return false;
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email.toLowerCase());
}

export const loginLocal = (email: string) => window.localStorage.setItem(EID_KEY, email);

export const logoutLocal = () => window.localStorage.removeItem(EID_KEY);

export const getUserEmail = () => window.localStorage.getItem(EID_KEY) || "";

export const createUuid = () => 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    let r = Math.random() * 16 | 0, v = c === 'x' ? r : ((r & 0x3) | 0x8);
    return v.toString(16);
 });