import { TextareaHTMLAttributes } from 'react';

interface IProps extends TextareaHTMLAttributes<HTMLTextAreaElement> {
    label?: string,
    error?: string
}

const TextArea: React.FC<IProps> = ({ label, error, ...attrs }) => (
    <div className="form-div">
        {label && <label className="form-label">{label}</label>}
        <textarea className={`form-item form-area${error ? ' error' : ''}`} {...attrs} />
    </div>
);

export default TextArea;