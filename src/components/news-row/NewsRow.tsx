import { NewsItem } from '..';
import { INews } from '../../interfaces';
import './styles.css';

interface IProps {
    heading: string,
    newsArr: INews[]
}

const NewsRow = ({ heading, newsArr }: IProps) => {
    return (
        <div className="news-row shadow">
            <div className="hdng-ctr">
                <span className="hdng">
                    {heading}
                </span>
            </div>
            {newsArr.map((news, key) => <NewsItem key={key} news={news} />)}
        </div>
    );
}

export default NewsRow;