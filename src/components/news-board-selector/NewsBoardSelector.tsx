import { IBoard } from '../../interfaces';
import './styles.css';

interface IProps {
    boards: IBoard[],
    onSelectBoard: (e: React.ChangeEvent<HTMLSelectElement>) => void
}

const NewsBoardSelector = ({ boards, onSelectBoard }: IProps) => (
    <div className="selector-ctr">
        <h1>News Board Selector</h1>
        <select className="board-selector" onChange={onSelectBoard}>
            {boards.map((board, key) => <option key={key} value={board.id}>{board.name}</option>)}
        </select>
    </div>
);

export default NewsBoardSelector;