import React from "react";

const BoardContext = React.createContext({
    boardId: '',
    fetchBoardContent: (s: string) => { }
});

export default BoardContext;