import { ChangeEvent, FormEvent, useState } from 'react';
import { INews, ENewsStatus } from '../../interfaces';
import { isValidEmail } from '../../utils/commonFunctions';
import useForm from '../../utils/hooks/useForm';
import { API_BASE_URL } from '../../constants';
import api from '../../utils/api';
import {
    TextInput,
    TextArea,
    Button,
} from '../atomic';
import './styles.css';

interface IProps {
    news: INews,
    closeUpdate: () => void,
    refetchData: () => void
}
interface IFormState {
    "title": string,
    "imageURL": string,
    "description": string,
    "author": string
}
interface IError {
    "title": string,
    "imageURL": string,
    "description": string,
    "author": string
}

const ERRORS_INIT = {
    "title": "",
    "imageURL": "",
    "description": "",
    "author": "",
};

const NewsEditor = ({ news, closeUpdate, refetchData }: IProps) => {
    const { form, onChangeForm } = useForm({
        "title": news.title || '',
        "imageURL": news.imageURL || '',
        "description": news.description || '',
        "author": news.author || '',
    })
    const [error, setError] = useState({ ...ERRORS_INIT });

    const onChange = (e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLSelectElement> | ChangeEvent<HTMLTextAreaElement>) => {
        onChangeForm(e);
        setError({ ...ERRORS_INIT });
    };

    const onSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (!isValidEmail(form.author)) {
            setError({
                ...error,
                author: "Invalid Email"
            });
            return;
        }
        if (news.status === ENewsStatus.published) {
            let isError = false;
            let errorUpdated: IError = { ...ERRORS_INIT };
            ["title", "description", "imageURL"].forEach(field => {
                if (!form[field as keyof IFormState]) {
                    isError = true;
                    errorUpdated[field as keyof IError] = `This field can't be empty`;
                }
            })
            if (isError) {
                setError(errorUpdated);
                return;
            }
        }
        const body = {
            ...news,
            ...form
        }
        updateNews(body);
    }

    const updateNews = async (news: INews) => {
        const { error, ok, statusText } = await api.put(`${API_BASE_URL}news`, news);
        if (error || !ok) {
            alert(statusText);
            closeUpdate();
            return;
        }
        refetchData();
    }

    const onCancel = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault();
        closeUpdate();
    }

    return (
        <div>
            <form onSubmit={onSubmit}>
                <TextInput
                    label="Author"
                    name="author"
                    onChange={onChange}
                    value={form.author}
                    error={error.author}
                />
                <TextInput
                    label="Title"
                    name="title"
                    onChange={onChange}
                    value={form.title}
                    error={error.title}
                />
                <TextArea
                    label="Description"
                    name="description"
                    onChange={onChange}
                    value={form.description}
                    error={error.description}
                />
                <TextInput
                    label="Image URL"
                    name="imageURL"
                    onChange={onChange}
                    value={form.imageURL}
                    error={error.imageURL}
                />
                <div className="btn-ctr">
                    <Button onClick={onCancel} text="Cancel" />
                    <Button text="Update" />
                </div>
            </form>
        </div>
    );
}

export default NewsEditor;