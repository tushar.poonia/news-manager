import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { isValidEmail, loginLocal, logoutLocal, getUserEmail } from './utils/commonFunctions';

describe('tests common functions', () => {

  describe('validates email ID', () => {

    test('Empty email ID', () => {
      expect(isValidEmail('')).toBe(false);
    })

    test('Invalid email ID', () => {
      expect(isValidEmail('abc@def')).toBe(false);
    })

    test('Valid email ID', () => {
      expect(isValidEmail('abc@def.com')).toBe(true);
    })

  })

  describe('logs user in', () => {
    afterAll(() => {
      logoutLocal();
    })

    test('login', () => {
      const userEmail = 'abc@def.com';
      expect(getUserEmail()).toBe("");
      loginLocal(userEmail);
      expect(getUserEmail()).toBe(userEmail);
    })

  })

  describe('logs user out', () => {
    const userEmail = 'abc@def.com';
    beforeAll(() => {
      loginLocal(userEmail);
    })

    afterAll(() => {
      logoutLocal();
    })

    test('logout', () => {
      expect(getUserEmail()).toBe(userEmail);
      logoutLocal();
      expect(getUserEmail()).toBe("");
    })

  })

})

describe('tests views', () => {

  describe('Login screen tests', () => {
  
    beforeEach(() => {
      logoutLocal();
      render(<App />);
    })
  
    afterAll(() => {
      logoutLocal();
    })
  
    test('renders login screen', () => {
      expect(screen.getByTestId('login')).toBeInTheDocument();
    })
  
    test('login button is disabled', () => {
      const loginButton = screen.getByTestId('login-button') as HTMLButtonElement;
      expect(loginButton).toBeInTheDocument();
      expect(loginButton.disabled).toBe(true);
    })
  
    test('renders Home screen on login', () => {
      new Promise(() => loginLocal('abc@def.com'))
        .then(() => expect(screen.getByTestId('home')).toBeInTheDocument())
    })
  
  })
  
  describe('Home screen tests', () => {
  
    beforeEach(() => {
      loginLocal('abc@def.com');
      render(<App />);
    })
  
    afterAll(() => {
      logoutLocal();
    })
  
    test('renders Home screen', () => {
      expect(screen.getByTestId('home')).toBeInTheDocument();
    })
  
    test('renders Login screen on logout', () => {
      new Promise(() => logoutLocal())
        .then(() => expect(screen.getByTestId('login')).toBeInTheDocument())
    })
  
  })

})