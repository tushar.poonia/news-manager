import React, { ButtonHTMLAttributes } from 'react';
import './styles.css'

interface IProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    text: string,
    addClass?: string
}

const Button: React.FC<IProps> = ({ text, addClass, ...attrs }) => (
    <button className={`btn${addClass ? ` ${addClass}` : ''}`} {...attrs}>{text}</button>
);

export default Button;