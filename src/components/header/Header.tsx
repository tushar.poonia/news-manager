import { Button } from '../atomic';
import './styles.css';

interface IProps {
    currUser: string | null,
    logout: () => void,
}

const Header = ({ currUser, logout }: IProps) => (
    <header>
        <img
            className="logo"
            alt="upday-logo"
            src="https://images.squarespace-cdn.com/content/v1/56950288bfe87314267ad53c/1465381565046-T8J13UACNVA73GW2M6GL/upday.png?format=2500w" />
        {currUser && <Button text="Logout" onClick={logout} />}
    </header>
);

export default Header;