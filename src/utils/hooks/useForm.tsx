import { ChangeEvent, useCallback, useState } from 'react';
import { IObject } from '../../interfaces';

const useForm = <T extends IObject>(initialValues: T) => {
    const [form, setForm] = useState(initialValues);

    const reInitForm = useCallback(() => {
        setForm(initialValues)
    }, [initialValues])

    const onChangeForm = (e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLSelectElement> | ChangeEvent<HTMLTextAreaElement>) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        });
    }

    const resetForm = () => {
        setForm(initialValues);
    }

    return { form, onChangeForm, resetForm, reInitForm };
}

export default useForm;