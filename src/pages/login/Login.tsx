import React, { useState } from 'react';
import { Button, TextInput } from '../../components/atomic';
import { isValidEmail } from '../../utils/commonFunctions';
import useForm from '../../utils/hooks/useForm';
import './styles.css';

interface IProps {
    login: (email: string) => void,
}

const Login = ({ login }: IProps) => {
    const { form, onChangeForm } = useForm({ email: '' });
    const [disabled, setDisabled] = useState(true);

    const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const userInput = e.target.value;
        onChangeForm(e);
        if (isValidEmail(userInput) && disabled) {
            setDisabled(false);
        } else if (!isValidEmail(userInput) && !disabled) {
            setDisabled(true);
        }
    }

    const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const { email } = form;
        if (disabled || !isValidEmail(email)) {
            return;
        }
        login(email);
    }

    return (
        <div className="login" data-testid="login">
            <div>
                <form onSubmit={onSubmit}>
                    <div>
                        <TextInput
                            type="email"
                            name="email"
                            placeholder="Please enter your email ID to login"
                            onChange={onChange}
                            value={form.email}
                            data-testid="login-input"
                        />
                    </div>
                    <div className="btn-login-ctr">
                        <Button
                            addClass="btn-login"
                            text="Login"
                            disabled={disabled}
                            data-testid="login-button"
                        />
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Login;