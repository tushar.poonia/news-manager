export interface IBoard {
    id: string,
    name: string
}

export interface IBoardContent {
    drafts?: INews[],
    published?: INews[],
    archives?: INews[],
}

export interface INews {
    id: string,
    boardId: string,
    author: string,
    title: string,
    description: string,
    imageURL: string,
    createdAt: string,
    status: string
}

export enum ENewsStatus {
    drafts = "drafts",
    published = "published",
    archived = "archived",
    archive = "archive"
}

export interface IObject {
    [key: string]: any
}

export enum EApi {
    GET = "GET",
    POST = "POST",
    PUT = "PUT",
    DELETE = "DELETE",
}