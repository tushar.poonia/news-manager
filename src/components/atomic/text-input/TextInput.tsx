import { InputHTMLAttributes } from 'react';

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
    label?: string,
    error?: string
}

const TextInput: React.FC<IProps> = ({ label, error, ...attrs }) => (
    <div className="form-div">
        {label && <label className="form-label">{label}</label>}
        <input className={`form-item form-input${error ? ' error' : ''}`} {...attrs} />
    </div>
);

export default TextInput;