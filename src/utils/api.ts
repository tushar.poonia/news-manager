import { EApi, IObject } from "../interfaces";

interface IOptions {
    headers: IObject,
    method: keyof typeof EApi,
    body?: string
}

const headers = {
    'Content-Type': 'application/json'
};
const ERROR_RES = {
    ok: false,
    data: null,
    status: 404,
    statusText: 'Not Found',
    error: true
};

const callApi = async (url: string, method: keyof typeof EApi, body: IObject = {}) => {
    try {
        const options: IOptions = {
            headers,
            method
        }
        if(method !== EApi.GET) {
            options.body = JSON.stringify(body);
        }
        const response = await fetch(url, options);
        let data;
        try {
            data = await response.json();
        } catch (e) {
            data = null;
        }
        const { ok, status, statusText } = response;
        return {
            ok,
            data,
            status,
            statusText,
            error: false
        };
    } catch (e) {
        console.error(e);
        return ERROR_RES;
    }
}

const api = {
    get: async (url: string) => await callApi(url, EApi.GET),
    delete: async (url: string) => await callApi(url, EApi.DELETE),
    post: async (url: string, body: IObject = {}) => await callApi(url, EApi.POST, body),
    put: async (url: string, body: IObject = {}) => await callApi(url, EApi.PUT, body),
};

export default api;