import { useContext, useState } from 'react';
import { INews, ENewsStatus } from '../../interfaces';
import { NewsEditor } from '..';
import BoardContext from '../../utils/BoardContext';
import { API_BASE_URL, NEWS_TYPES } from '../../constants';
import { isValidEmail } from '../../utils/commonFunctions';
import api from '../../utils/api';
import './styles.css';
import { Button } from '../atomic';

interface IProps {
    news: INews
}

const NewsItem = ({ news }: IProps) => {
    const [isEdit, setIsEdit] = useState(false);
    const [newsStatus, setNewsStatus] = useState(news.status);
    const { boardId, fetchBoardContent } = useContext(BoardContext);

    const closeUpdate = () => {
        setIsEdit(false);
    }

    const onChangeNewsStatus = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setNewsStatus(e.target.value);
    }

    const saveNewsStatus = async () => {
        if (isDisabled() || news.status === newsStatus) {
            return;
        }
        const url = `${API_BASE_URL}news/${news.id}/${newsStatus}`;
        const { error, ok, statusText } = await api.post(url);
        if (error || !ok) {
            alert(statusText);
            return;
        }
        refetchData();
    }

    const onDelete = async () => {
        const consent = window.confirm('Are you sure that you want to delete this news item?');
        if (consent) {
            const url = `${API_BASE_URL}news/${news.id}`;
            const { error, ok, statusText } = await api.delete(url);
            if (error || !ok) {
                alert(statusText);
                return;
            }
            refetchData();
        }
    }

    const refetchData = () => {
        if (boardId && fetchBoardContent) {
            fetchBoardContent(boardId);
        }
    }

    const isDisabled = () => {
        const { title, imageURL, description, author } = news;
        return newsStatus === ENewsStatus.published &&
            (!title || !imageURL || !description || !isValidEmail(author));
    }

    return (
        <div className="news-item">
            <div className="control-ctr">
                <div className="title">
                    {news.title}
                </div>
                <div className="controls">
                    {!isEdit &&
                        <Button
                            addClass="controller-btn"
                            text="Edit"
                            onClick={() => setIsEdit(true)}
                        />}
                    <Button
                        addClass="controller-btn"
                        text="Delete"
                        onClick={onDelete}
                    />
                </div>
            </div>
            {
                isEdit ?
                    <NewsEditor
                        news={news}
                        closeUpdate={closeUpdate}
                        refetchData={refetchData}
                    /> :
                    <div className="content">
                        <div className="author item-div">
                            By {news.author}
                        </div>
                        <div className="img item-div">
                            {news.imageURL &&
                                <img
                                    className="img-item"
                                    alt=""
                                    src={news.imageURL}
                                />}
                        </div>
                        <div className="desc item-div">
                            {news.description}
                        </div>
                        <div className="board">
                            {news.status !== ENewsStatus.archived &&
                                <div className="status item-div">
                                    <select
                                        className="select-status"
                                        name="status"
                                        onChange={onChangeNewsStatus}
                                        value={newsStatus}
                                    >
                                        {NEWS_TYPES.map((status, key) => (
                                            <option key={key} value={status}>
                                                {status}
                                            </option>
                                        ))}
                                    </select>
                                    <Button
                                        text="Save"
                                        disabled={isDisabled()}
                                        onClick={saveNewsStatus}
                                    />
                                    {isDisabled() &&
                                        <div className="error-fill">Fill all the required fields before Publishing</div>}
                                </div>}
                        </div>
                    </div>
            }
        </div>
    );
}

export default NewsItem;