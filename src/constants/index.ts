import { ENewsStatus } from '../interfaces';

export const NEWS_TYPES = [ENewsStatus.drafts, ENewsStatus.published, ENewsStatus.archive];
export const EID_KEY = "eid";
export const API_BASE_URL = 'http://localhost:8080/v1/';